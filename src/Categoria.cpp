#include <iostream>
#include <string>
#include "Categoria.hpp" 

using namespace std;

Categoria::Categoria () {
    set_nome_categoria("");
    set_codigo(0);

}

Categoria::~Categoria () {
   
}

void Categoria::set_nome_categoria (string nome_categoria){
    this -> nome_categoria = nome_categoria;
}

string Categoria::get_nome_categoria (){
    return nome_categoria;
}

void Categoria::set_codigo (int codigo){
    this -> codigo = codigo;
}

int Categoria::get_codigo (){
    return codigo;
}

void Categoria::cadastrar(){

    cout << "  CADASTRO DE CATEGORIA  " << endl;
    cout << "NOME DA CATEGORIA: " << endl;
    cin >> nome_categoria;
    cout << "CODIGO DA CATEGORIA: " << endl;
    cin >> codigo;
    
}

void Categoria::alterar(){

    

    cout << "  MODIFICAR CATEGORIA  " << endl;
    cout << "DIGITE O CODIGO DA CATEGORIA A SER ALTERADA: " << endl;
    cin >> codigo;
    
    
}

void Categoria::pesquisar(){


    cout << "  PESQUISAR CATEGORIA  " << endl;
    cout << "DIGITE O CODIGO DA CATEGORIA A SER PESQUISADA: " << endl;
    cin >> codigo;

}

void Categoria::atualizar(){
    
    cout << "  ATUALIZAR CATEGORIA  " << endl;
    cout << "DIGITE O CODIGO DA CATEGORIA A SER ATUALIZADA: " << endl;
    cin >> codigo;
    
}

 void Categoria::excluir(){

    
    cout << "  EXCLUIR CATEGORIA  " << endl;
    cout << "DIGITE O CODIGO DA CATEGORIA A SER EXCLUIDA: " << endl;
    cin >> codigo;

 }
