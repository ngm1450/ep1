
//Aluno: Nícolas Georgeos Mantzos
//Matrícula: 17/0051277
#include <iostream>
#include "Categoria.hpp"
#include "Funcionario.hpp"
#include "Produto.hpp"
#include "Cliente.hpp"
#include "ProdutoGenerico.hpp"
#include "Pessoa.hpp"
#include <vector>
#include<bits/stdc++.h>

using namespace std;

int main () {
    vector<Cliente * >lista_cliente;
    vector<Produto * >lista_produto;
    vector<Funcionario * >lista_funcionario;
    int opcao;
    Cliente cliente;
    Produto produto;
    Funcionario funcionario;

    while (1) {
   
        cout << "ESCOLHA UMA DAS OPCOES ABAIXO DIGITANDO SEU NUMERO CORRESPONDENTE: " << endl;
        cout << "1 - MODO VENDA" << endl;
        cout << "2 - MODO ESTOQUE" << endl;
        cout << "3 - MODO RECOMENDAÇÃO" << endl;
        cout << "0 - SAIR" << endl;
        cout << "INFORME A OPCAO: " << endl;

        cin >> opcao;

        if (opcao == 0) {
            break;
        }

         switch (opcao) {

            case 1: {

                
                cliente.cadastrar(lista_cliente);
                break;
               }
            
    
            case 2: {
                
                funcionario.cadastrar(lista_funcionario);
                produto.cadastrar(lista_produto);
                break;
             } 

         }  
       
    }

}

